package gamesys;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;

/*
 *  Author:     bumerrrang
 *  Project:    gamesysTest
 *  Package:    gamesys
 *  File:       DbTest
 *  Datetime:   8.08.18, 13:01
 *
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class DbTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private RSSJdbcRepository rssJdbcRepository;

    @Test
    public void findByTitle() {
        // given
        RSS rss = new RSS(2L, "test2");
        entityManager.persist(rss);
        entityManager.flush();

        // when
        RSS found = rssJdbcRepository.findByTitle(rss.getTitle());

        // then
        assertThat(found.getTitle())
                .isEqualTo(rss.getTitle());
    }
}
