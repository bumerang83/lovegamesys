package gamesys;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RSSTest {

    private RSS rss;

    @Before
    public void setUp() throws Exception {
        this.rss = new RSS(3L, "test");
    }

    @Test
    public void getId() {
        assertEquals((Long)3L, this.rss.getId());
    }

    @Test
    public void setId() {
        this.rss.setId(4L);
        assertEquals((Long)4L, this.rss.getId());
    }

    @Test
    public void getTitle() {
        assertEquals("test", this.rss.getTitle());
    }

    @Test
    public void setTitle() {
        this.rss.setTitle("test2");
        assertEquals("test2", this.rss.getTitle());
    }

    @Test
    public void compareTo() {
        RSS a = new RSS(3L, "test3");
        assertEquals(new RSS(2L, "test2").compareTo(new RSS(3L, "test3")), -1);
    }
}