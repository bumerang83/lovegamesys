package gamesys;

import gamesys.RSS;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/*
 *  Author:     bumerrrang
 *  Project:    gamesysTest
 *  Package:    gamesys
 *  File:       RSSRowMapper
 *  Datetime:   8.08.18, 09:37
 *
 */
public class RSSRowMapper implements RowMapper<RSS> {
    @Override
    public RSS mapRow(ResultSet rs, int rowNum) throws SQLException {
        RSS rss = new RSS();
        rss.setId(rs.getLong("id"));
        rss.setTitle(rs.getString("title"));
        return rss;
    }
}
