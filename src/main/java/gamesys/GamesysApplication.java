package gamesys;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GamesysApplication implements CommandLineRunner {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${pause}")
    private int pause;
    @Value("${url}")
    private String url;

    @Autowired
    RSSJdbcRepository repository;

    @Autowired
    RSSReader rssReader;

    public static void main(String[] args) {
        SpringApplication.run(GamesysApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        logger.info("RSSReader -> {}", rssReader.processXML(url));
/*
        while (true) {
            repository.insertRss(rssReader.processXML(url));
            Thread.sleep(pause);
        }
*/
    }
}
