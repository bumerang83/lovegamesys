package gamesys;

import javax.persistence.*;
import javax.validation.constraints.Size;

/*
 *  Author:     bumerrrang
 *  Project:    gamesysTest
 *  Package:    gamesys
 *  File:       RSS
 *  Datetime:   8.08.18, 09:29
 *
 */
@Entity
@Table(name = "gamesys_test")
public class RSS implements Comparable {
    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    @Size(min = 1, max = 255)
    private String title;

    public RSS() {
        super();
    }


    public RSS(String title) {
        super();
        this.title = title;
    }

    public RSS(Long id, String title) {
        super();
        this.setId(id);
        this.setTitle(title);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int compareTo(Object o) {
        RSS rss = (RSS)o;
        return this.id.compareTo(rss.id);
    }
}
