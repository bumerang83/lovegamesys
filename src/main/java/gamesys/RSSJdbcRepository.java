package gamesys;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/*
 *  Author:     bumerrrang
 *  Project:    gamesysTest
 *  Package:    gamesys
 *  File:       RSSJdbcRepository
 *  Datetime:   8.08.18, 09:42
 *
 */
@Repository
public class RSSJdbcRepository implements JpaRepository<RSS, Long> {
    @Autowired
    JdbcTemplate jdbcTemplate;

    public void insertRss(String rss) {
        String[] insertRss = rss.split("\n");
        for (String buf : insertRss) {
            jdbcTemplate.update("insert into gamesys_test (title) " + "values(?)",
                    new Object[]{buf + " -> Logic added manually ÖÄÜÕöäüõ"});
        }
    }

    public Stream<RSS> selectLast() {
        return jdbcTemplate.query("select TOP 10 * from gamesys_test ORDER BY id DESC ", new RSSRowMapper()).stream().sorted();
    }

    public RSS findByTitle(String title) {
        return jdbcTemplate.queryForObject("select * from gamesys_test where title=?", new Object[]{title},
                new BeanPropertyRowMapper<>(RSS.class));
    }

    @Override
    public List<RSS> findAll() {
        return null;
    }

    @Override
    public List<RSS> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<RSS> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<RSS> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(RSS rss) {

    }

    @Override
    public void deleteAll(Iterable<? extends RSS> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends RSS> S save(S s) {
        return null;
    }

    @Override
    public <S extends RSS> List<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<RSS> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends RSS> S saveAndFlush(S s) {
        return null;
    }

    @Override
    public void deleteInBatch(Iterable<RSS> iterable) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public RSS getOne(Long aLong) {
        return null;
    }

    @Override
    public <S extends RSS> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends RSS> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends RSS> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends RSS> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends RSS> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends RSS> boolean exists(Example<S> example) {
        return false;
    }
}