package gamesys;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Stream;

/*
 *  Author:     bumerrrang
 *  Project:    gamesysTest
 *  Package:    gamesys
 *  File:       LovegamesysController
 *  Datetime:   8.08.18, 10:07
 *
 */
@RestController
public class LovegamesysController {

    @Autowired
    RSSJdbcRepository repository;

    @RequestMapping("/lovegamesys")
    public Stream<RSS> gamesys() {
        return repository.selectLast();
    }

    @RequestMapping("/test")
    public String test() {
        return "Hello, World!";
    }
}